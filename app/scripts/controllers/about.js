'use strict';

/**
 * @ngdoc function
 * @name cynergyApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the cynergyApp
 */
angular.module('cynergyApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
