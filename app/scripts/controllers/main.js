'use strict';

/**
 * @ngdoc function
 * @name cynergyApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the cynergyApp
 */
angular.module('cynergyApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
