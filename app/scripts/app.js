'use strict';

/**
 * @ngdoc overview
 * @name cynergyApp
 * @description
 * # cynergyApp
 *
 * Main module of the application.
 */
angular
  .module('cynergyApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'sticky'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/industrial-consulting', {
        templateUrl: 'views/industrial-consulting.html',
        controller: 'ConsultingCtrl'
      })
      .when('/renewable-energy-research', {
        templateUrl: 'views/renewable-energy-research.html',
        controller: 'ResearchCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
